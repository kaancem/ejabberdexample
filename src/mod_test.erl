-module(mod_test).

-behaviour(gen_mod).

%% Required by ?INFO_MSG macros
-include("logger.hrl").

%% gen_mod API callbacks
-export([start/2, stop/1]).

-export([filter_packet_handler/1]).


start(_Host, _Opts) ->
    ?INFO_MSG("Starting the test module...", []),

    ejabberd_hooks:add(filter_packet, global, ?MODULE, filter_packet_handler, 0),

    ?INFO_MSG("Test module started.", []),
    ok.

stop(_Host) ->
    ?INFO_MSG("Stoping the test module...", []),

    ejabberd_hooks:delete(filter_packet, global, ?MODULE, filter_packet_handler, 0),

    ?INFO_MSG("Test module stopped.", []),
    ok.


filter_packet_handler({FromUser, ToUser, XML} = Packet) ->
    ?INFO_MSG("Packet received: From: ~p To: ~p XML: ~p.", [FromUser, ToUser, XML]),
    Packet.